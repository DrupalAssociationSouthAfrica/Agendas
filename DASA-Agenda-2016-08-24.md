Drupal Association South Africa
===============================

Agenda 2016-08-24 from 19:30
----------------------------

[Hangout](https://plus.google.com/hangouts/_/dasa.org.za/dasa)

1.1. Attending / Apologies
--------------------------

    Person    Attending ✔ / ✘ Apologies
    ===================================

    Governing Board

    Greg McKeen             ✔
    Ingrid (Inky) Talbot    ✔
    Jason Lewis             ✔
    Johan du Bois           ✔
    Lee Taylor              ✘
    Riaan Burger            ✘

    Guest Attendees - None


Quorum Achieved: ✔

A quarter of the members of the governing board. CIEL(6/4) = at least 2 members.

Apologies:
	Riaan Burger
	Lee Taylor


--------------------------------------------------------------------------------

1.2. Review & Acceptance of Minutes
-----------------------------------

Minutes of Previous Meeting(s): 
- https://gitlab.com/DrupalAssociationSouthAfrica/Agendas/blob/master/DASA-Agenda-2016-07-20.md
- https://gitlab.com/DrupalAssociationSouthAfrica/Agendas/raw/master/DASA-Agenda-2016-08-10.md

Minutes:

  - Proposed: Greg
  - Seconded: Jason

- Minutes of the previous meeting were discussed
- Johan will commit the changes as agreed upon

Completed / Agreed: ✔

--------------------------------------------------------------------------------

1.3. Confirm Next Meeting
-------------------------

Minutes:

- Wednesday 21st September, 7:30pm, Online

Completed / Agreed: ✔

================================================================================

2. Finances
===========

--------------------------------------------------------------------------------

2.1. General
------------
- SAGE Online reconciled with bank account. Reports available in Internal folder on Google Drive.
  - Balance: R 66,121.51
  - DCG2016 Sponsorships Outstanding: R 5,000 
- Confirmation of change in banking forwarded to FNB. 

Minutes:

-

Completed / Agreed: ✔ / ✘

--------------------------------------------------------------------------------

2.2. DrupalCamp Gauteng 2016: Sponsorship Outstanding
----------------------------------------------------

Agenda:
- Johan: Final payment from SYW received. Statement with balance uploaded to Google Docs folder.
- Riaan: Followed up with Yonder, payment forthcoming.

Some sponsors have not paid yet:

- Yonder, R 5 000
- Total: **R 5 000**

Minutes:
-

Completed / Agreed: ✔

--------------------------------------------------------------------------------

2.3. Bookkeeping
----------------

Johan: No suggested book keepers received. Have identified some in CT but not yet contacted. Will advise by email. 

Minutes:

- Johan will get back to everyone via email once there is something to report


================================================================================

3. Drupal Camp Cape Town 2016
===============================================

Minutes:
- Planning going well
- XTeam/Springfisher on board as Gold Sponsor
- LAB signed up as Platinum Sponsor
- Entertainment has been sorted
- Potential Beer sponsor
- No expenses yet



3.1. Cape Town Meetups
------------
Minutes:
- 31 August, Bryan Gruneberg will be speaking
- New caterer
- Looking for September Speaker

================================================================================

4. AGM 
===================

--------------------------------------------------------------------------------

4.1. Legal Advice
------------

- Johan: pending; will advise by email. This process can run parallel to online elections. 

Minutes:

- Johan will get back to everyone via email once there is something to report

Completed / Agreed: ✔ 

4.2. Elections
------------

- Riaan: Proceed with as agreed. Starting with GDoSA annoucement.
- Johan: Simplified timeline (based on 2016-07-20 agenda):

-  November:  AGM/Formal Handover to New Governing Board (notice as per constitution)
- <31 October:   Publish results
-  24 October:   Close voting
-  19 September: Open voting
-  ? September:
  - Close acceptance of new candidates.
  - Administrative: Gather short bio of all candidates, prepare to open vote.
-  ? August - ? September: Call for candidates.
  - Post GDoSA
  - Email/Tweet/etc. referring to post on GDoSA

Minutes:

- Open the call for candidates: 31st August
- Close of candidates: 21st September
- Week to sort out bios, etc.
- Open voting 28th September
- Voting open for 3 weeks until 19th October 23:59 (midnight)
- Publish results by 31st October

Completed / Agreed: ✔

================================================================================

5. Any Other Business
=====================

Agenda Notes:

- Anything arising at the meeting or not on the agenda.

Minutes:

-

Completed / Agreed: ✔ / ✘

================================================================================

6. Not Broadcast or Recorded
============================

--------------------------------------------------------------------------------

Any Sensitive Topics *Everyone*
-------------------------------

Agenda Notes:

- Any topics that need to be held in confidence.
- No recording and minutes to be kept in print / writing only.
- Example of such items are ones discussed with relation to our constitution:
  - 7: Governing Board
    - 7.11: Confidentiality
      - All matters pertaining to litigation, security measures, contractual
        negotiations, employment matters and any other matters deemed
        onfidential by the Governing Board, must be treated as confidential
        and only the actual decisions may be disclosed to the general public.

Minutes:

-

Completed / Agreed: ✔ / ✘

================================================================================

7. Template Vote Block
======================

Voted on: ((Statement))

    Greg McKeen             ✔ / ✘
    Ingrid (Inky) Talbot    ✔ / ✘
    Jason Lewis             ✔ / ✘
    Johan du Bois           ✔ / ✘
    Lee Taylor              ✔ / ✘
    Riaan Burger            ✔ / ✘

================================================================================
