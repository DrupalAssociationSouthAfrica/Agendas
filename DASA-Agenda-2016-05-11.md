# Drupal Association South Africa

## Agenda 2016-05-11 19:00

[Hangout](https://plus.google.com/hangouts/_/dasa.org.za/dasa)

--------------------------------------------------------------------------------

# 1. General

## 1.1. Attendance

    Person    Attending ✔ / ✘ Apologies

    Governing Board

    Greg McKeen             ✔
    Ingrid Talbot           ✔
    Jason Lewis             ✔
    Johan du Bois           ✘
    Lee Taylor              ✔
    Renate Ehlers           ✔
    Riaan Burger            ✔

    Quorum achieved (five of governing board): ✔

Apologies: Johan 

Regular/Guest Attendees: Mark

Minutes:


--------------------------------------------------------------------------------

## 1.2 Review & Acceptance of Minutes

- [ ] Approval of [minutes of previous meeting](https://gitlab.com/DrupalAssociationSouthAfrica/Agendas/blob/master/DASA-Agenda-2016-04-13.md)
- [ ] Acceptance of Agenda/Matters arising.

Minutes:
Accepted by Renate, seconded by Lee

--------------------------------------------------------------------------------

## 1.3. Review of Assigned Tasks

- [ ] Each individual reports back on tasks assigned to them
  (as per task list/previous minutes).

Minutes:

--------------------------------------------------------------------------------

## 1.4. Confirm Next Meeting

Minutes:
AGM being held in June
July 13th next meeting

--------------------------------------------------------------------------------

# 2. Chair's Report

## 2.1. Annual General Meeting

- AGM to be held by teleconference on Saturday, 11 June 2016, from 09:00 to
  15:00.
  - Attendees in Cape Town & Gauteng will gather at designated teleconferencing
    venues in CT and JHB respectively.
  - Submission date for draft reports & resolutions: Monday, 30 May 2016
  - Budget: R2000
  - Agenda to include:
    - Annual narrative report & financial statements;
    - Constitutional amendments (if required)
    - Policy amendments
    - Portfolio Definions
    - Adoption of Policy Guideline Wiki
  - Annual planning & bugetting session

Tasks:

- [ ] Confirm teleconferencing venues
- [ ] Agenda to be put together - for AGM and for planning

Minutes:
- Native to do catering for the day.
- Numbers to be finalised by 30th May.
- Facilitator 
-- a corporate person should not be used as a facilitator
-- prefer to find someone from an open source community (Riaan)
-- Agreed that a facilitator would be useful
-- Purpose of a facilitator to be looked into
-- To be looked into further once planning agenda has been decided

--------------------------------------------------------------------------------

## 2.2. Indicators Report

### Riaan

- Indicators for April are available at https://gitlab.com/DrupalAssociationSouthAfrica/Indicators/tree/master/indicators

Tasks:

- [ ] Note April metrics and discuss if required.
- [ ] Confirm whether Riaan has received headcount from meetup organisers as
  agreed.

Minutes:
- Nothing new, all as per the link above

--------------------------------------------------------------------------------

# 3. Secretary's Report

## 3.1. Membership

- Status of register of members (Ingrid)
- Memberships due for renewal (Riaan)

Tasks:

- [ ] Confirm register of members URL

Minutes:
- Nothing new from Ingrid
- Lance is currently up for renewal

--------------------------------------------------------------------------------

## 3.2. Record Keeping

- Status of the following records:
  - NPO certificate;
  - Original/signed constitution;
  - Agendas & minutes;
  - Correspondence.

Tasks:

- [ ] Remind members to forward important historical correspondence to
  info@dasa.org.za for archiving.
- [ ] Remind members to CC/forward important correspondence to info@dasa.org.za
  for archiving.

Minutes:
- Constitution to be signed to DC Gauteng, so that Jason can bring it back to Cpt

---

## 3.3 DASA Calendar

- We resolved on 2014-01-15 that DASA have a shared Google Calendar, later
  meetings added that we are jointly responsible for keeping it up to date, and
  that we should all have access. The calendar is available as HTML at
  https://calendar.google.com/calendar/embed?src=dasa%40dasa.org.za&ctz=Africa/Johannesburg
- This is the calendar that will be consulted when planning DASA event, for
  example, to avoid conflicts. Please keep it up to date. Meetup Champions
  please take responsibility for your meetups.

Tasks:

- [ ] Round-robin checkin to confirm all members' events are captured.

Minutes:
- Members have been adding events to the calendar, Mark is still to add a few things

--------------------------------------------------------------------------------

## 3.4. System Administration

### Lee

- System administration for DASA is evolving as a portfolio. It includes:
  maintain list of all DASA systems & accounts; ensure storage, backup, security
  of DASA electronic records; and, maintain DASA domain name holdings.
  - System list and related documents maintained by Sys Admin.
  - Passwords held securely by Sys Admin.

Tasks:

- [ ] Confirm location/URL of systems list - https://drive.google.com/open?id=1mi407sTc9rVc9ldFEargqVSINxw-RyCGj6MamOrMYrA

Minutes:
- LastPass being used for remote access

--------------------------------------------------------------------------------

# 4. Treasurer's Report

- Treasurer's responsibilities include: implement financial controls, prepare
  budget, prepare annual financial statements, monthly reporting (actuals vs
  budget, exceptions).

## 4.1. Monthly Report

Minutes:
- Renate had to drop off the meeting.
- Available Balance: 60,451.32 CR
- Renate has been having issues with Sage Pastel, importing data

--------------------------------------------------------------------------------

## 4.2 Financial Statements 2015/16

Minutes:

--------------------------------------------------------------------------------

## 4.3 Budget 2016/17

Minutes:

--------------------------------------------------------------------------------

# 5. Meetups

## 5.1 Supporting Meetups

- Supporting the meetups is core to realising our vision.
- Meetups portfolio includes:
  - Custodian of our list of meetups (https://drive.google.com/open?id=1xCep2OaBMPAXvFV_SWEqvs_zQ6bAF9XFqpUj1mmiXoI);
  - Maintains list of meetups' needs and requests;
  - Custodian of speaker lists/database that meetups can draw from;
  - Gives a short summary report on meetups at these meetings.
- Meetup champions to have a champion who will:
  - Report back to this meeting and the Meetups portfolio holder on events held;
  - When possible gather slides, talk recordings, etc. and upload to shared drive;
  - Bring meetup's needs and requests to the team;
  - Ensure events for relevant meetup added to DASA Calendar.

Tasks:
- [ ] Confirm URL for speaker list

Minutes:
- Folder on Google Docs where we can upload photos from Meetups, members encouraged to add to it

--------------------------------------------------------------------------------

## 5.2 Pretoria

### Renate

Minutes:
- Work overload putting strain on attendance
- 3 attendees last month

--------------------------------------------------------------------------------

## 5.3 Cape Town

### Jason

Minutes:
- April back at Bandwidth Barn, 17 attendees
- May cancelled in favour of Gauteng Camp
- Next meetup in June

--------------------------------------------------------------------------------

## 5.4 Durban

### Mark

Minutes:
- Meeting every 3 months
- April had 7 attendees, 6 additional members on the Meetup group

--------------------------------------------------------------------------------

## 5.5 Johannesburg / Parkhurst

### Riaan

Minutes:
- Riaan hopes to put in personal effort to get it going again
- Hopes to revive it through making it more technical
- Possibility to rotate meetups between locations, 1 every 3 months in each location

--------------------------------------------------------------------------------

# 6. Events

## 6.1 DrupalCamp Gauteng

Minutes:
- http://gauteng2016.drupalcamp.co.za/
- 30% booked so far
- Currently using Drupal.org and Drupal Groups to advertise
- Social Media campaign not going to happen for this year - needs more time

## 6.2 DrupalCamp Cape Town

Minutes:
- New branding and website to be ready and live by May 26th

## 6.3 DrupalGovZA

--------------------------------------------------------------------------------

# 7. Communications

## 7.1 Drupal Groups: South Africa / groups.drupal.org/south-africa

## 7.2 Social Media

Minutes
- Need someone to take over Facebook account and push that platform
- Twitter - DrupalSA, getting users to migrate to it

## 7.3 Website

## 7.4 Newsletter

--------------------------------------------------------------------------------

# 8. Any Other Business

Stood over from previous minutes:

- Riaan: email address so that members can reply to communications via a
  dasa.org address.

Minutes:
- Riaan suggested we pick up discussion around dasa.org email address on 11th June

- Riaan asks that Agenda be ready earlier in the month

--------------------------------------------------------------------------------

# 9. Not Broadcast or Recorded

Notes:

- Any sensitive topics that need to be held in confidence.
- No recording
- Minutes to be kept in writing only
- Example of such items are ones discussed with relation to our constitution:
  - 7: Governing Board
  - 7.11: Confidentiality
- All matters pertaining to litigation, security measures, contractual
  negotiations, employment matters and any other matters deemed confidential by
  the Governing Board, must be treated as confidential and only the actual
  decisions may be disclosed to the general public.

Minutes:

--------------------------------------------------------------------------------

# 10. Review Tasks Arising from this Meeting

## Secretary

A quick review of tasks assigned at this meeting. Tasks not completed from previous meetings will only be brought over after discussion.

### Johan
- [ ] Agenda to be put together - for AGM and for planning

### Ingrid

### Renate
- [ ] to have financials up to date for AGM

### Riaan
- [ ] Put forward consideration of how we can have a dasa.org email address to reply from 

### Jason

### Lee

### All

Minutes:

--------------------------------------------------------------------------------

