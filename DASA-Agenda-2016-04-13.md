# Drupal Association South Africa

## Agenda 2016-04-13 19:00 https://plus.google.com/hangouts/_/dasa.org.za/dasa
---
# 1. General
## 1.1. Attendance

    Person    Attending ✔ / ✘ Apologies
    ---

    Governing Board

    Greg McKeen             ✔
    Ingrid Talbot           ✔
    Jason Lewis             ✔
    Johan du Bois           ✔
    Lee Taylor              ✔
    Renate Ehlers           ✔
    Riaan Burger            ✔

    Quorum achieved (five of governing board): ✔

Apologies:

Regular/Guest Attendees:
	Mark Pape

Minutes:

---
## 1.2 Review & Acceptance of Minutes

- [x] Approval of minutes of previous meeting [https://gitlab.com/DrupalAssociationSouthAfrica/Agendas/blob/master/DASA-Agenda-2016-03-02.md]
- [X] Acceptance of Agenda/Matters arising

Minutes:
Jason and Ingrid accepted minutes

---
## 1.3. Review of Assigned Tasks

- [ ] Each individual reports back on tasks assigned to them (as per task list/previous minutes)

Minutes:
This is a new item on the agenda, last month's tasks to be added, and from next month this will be reviewed at the start of each meeting.

---
## 1.4. Confirm Next Meeting

Minutes:
Next meeting 11 May 2016, 7-9pm, added to DASA Google Calendar

---

# 2. Chair's Report
## 2.1. Annual General Meeting

- NPO registration and constitution require annual general meeting. It should be held within 6 months of end of financial year and includes presentation and adoption of annual narrative report & financials.
- Proposed: Full-day 28 May 2016 in Johannesburg
-- Agenda to include:
--- Annual narrative report & financial statements;
--- Constitutional amendments (if required)
--- Policy amendments
---- Portfolio Definions
---- Adoption of Policy Guideline Wiki
--- Annual planning & bugetting session

Tasks:
- [x] Agree on date for AGM: Saturday, 11 June 2016, 9am to 3pm
- [x] Agree on submission dates for draft reports & resolutions for AGM: Monday, 30 May 2016
- [x] Set guideline budget for AGM: R2000

Minutes:
Renate and Johan preferred to have an in-person meeting, but general consensus for 2016 decided that a tele-conference will be held.
The tele-conference will be held at one venue each for Cape Town and Johannesburg, possibly Durban. 
A different platform other than Google Hangouts will be used, to be investigated

___

## 2.2. Indicators Report
### Riaan

- Indicators for March are available at https://gitlab.com/DrupalAssociationSouthAfrica/Indicators/tree/master/indicators

Tasks:
- [X] Note March metrics and discuss if required.

Minutes:
Nothing major to be noted of
All meetup organisers to send Riaan actual headcount of meetups the day after 

---

# 3. Secretary's Report
## 3.1. Membership

- Status of register of members
- Memberships due for renewal

Minutes:
Ingrid to maintain register of members on Google Docs
Riaan currently manages memberships, when accounting system is set up properly a process between secretary and treasurer is to be put in place

___

## 3.2. Record Keeping

- Status of the following records:
-- NPO certificate;
-- Original/signed constitution;
-- Agendas & minutes;
-- Correspondence.

Tasks:
- [ ] Circulation of constitution for signature by 2015/16 governing board
    - Update, Print, Sign - Ingrid
- [ ] Ingrid & Renate’s Drupal.org details to be added
- [ ] Update the document to create version 1.0.1 - with “adopted on” date
    - [ ] Date to be confirmed

Minutes:
Any information needing to be kept for record keeping purposes - to be cc'd to info@dasa.org


---

## 3.3 DASA Calendar

- We resolved on 2014-01-15 that DASA have a shared Google Calendar, later meetings added that we are jointly responsible for keeping it up to date, and that we should all have access. The calendar is available as HTML at https://calendar.google.com/calendar/embed?src=dasa%40dasa.org.za&ctz=Africa/Johannesburg
- This is the calendar that will be consulted when planning DASA event, for example, to avoid conflicts. Please keep it up to date. Meetup Champions please take responsibility for your meetups.

Tasks:
- [ ] ? to co-ordinate everyone adding there items to the shared calendar.

Minutes:
Everyone to keep the calendar updated with their events

___

## 3.4. System Administration
### Lee

- System administration for DASA is evolving as a portfolio. It includes: maintain list of all DASA systems & accounts; ensure storage, backup, security of DASA electronic records; and, maintain DASA domain name holdings.
- Proposed: System list and related documents to be maintained on Google Docs.
- Proposed: Passwords to be held securily, as per Sys Admins, guidance: currently LastPass.

Tasks:
- [ ]

Minutes:
Accepted that a list will be created, and location to be determined
Passwords will be stored in LastPass

___

# 4. Treasurer's Report

- Treasurer's responsibilities include: implement financial controls, prepare budget, prepare annual financial statements, monthly reporting (actuals vs budget, exceptions).

## 4.1. Monthly Report

Minutes:

Still in the process of migrating, so no proper report for this month
Expenses: R0
Memberships received

___

## 4.2 Financial Statements 2015/16

Minutes:


___

## 4.3 Budget 2016/17

Minutes:

___

# 5. Meetups
## 5.1 Supporting Meetups

- Supporting the meetups is core to realising our vision.
- Proposed: Meetups portfolio be assigned to specific individual and include:
-- Custodian of our list of meetups (https://drive.google.com/open?id=1xCep2OaBMPAXvFV_SWEqvs_zQ6bAF9XFqpUj1mmiXoI);
-- Maintains list of meetups' needs and requests;
-- Custodian of speaker lists/database that meetups can draw from;
-- Gives a short summary report on meetups at these meetings.
- Proposed: Each meetup to have a champion who will:
-- Report back to this meeting and the Meetups portfolio holder on events held;
-- When possible gather slides, talk recordings, etc. and upload to shared drive;
-- Bring meetup's needs and requests to the team;
-- Ensure events for relevant meetup added to DASA Calendar.

Minutes:
Jason will take on custodian of meetups list, speakers
Riaan started a list a while back that can be used: https://docs.google.com/spreadsheets/d/1-kQxZI0t9NCG6VaBEDhksYL2MfsAGMfo-UhpA6K5_Ok/edit#gid=0

___

## 5.2 Pretoria
### Renate

Minutes:
Last month was a successful meetup.
About 20 attendees
Lots of new faces
Demographic needs to be expanded

___

## 5.3 Cape Town
### Jason

Minutes:
Held at Quirk as test run for DrupalCamp
29 in attendance
2 sponsored Drupal Certifications incentivised members to ask questions

---

## 5.4 Durban
### Mark

Minutes:
Starting up, will proceed once every quarter for 2016
Will bring in a case-study each meetup
8 attendees

---

## 5.5 Johannesburg
### Riaan

Minutes:
Will consider a social/drinks gathering between now and DrupalCamp Gauteng
Bryanston attendance is dropping which is a concern
Parkhurst doing well

___

# 6. Events

## 6.1 DrupalCamp Gauteng

- Open for sponsors, need two more platinum sponsors
- 2 speakers flying to South Africa, looking for more sponsors to cover costs
- Ticket sales open at R100 each
- 10 tickets sold so far of 150, no proper promotion has been put out as yet - until more sponsorship has been received

## 6.2 DrupalCamp Cape Town

- Venue and date confirmed
- More kickoff end of April
- Holding back on speaker request until Jhb is more under way

## 6.3 DrupalGovZA



___

# 7. Communications

## 7.1 Drupal Groups: South Africa

## 7.2 Social Media

## 7.3 Website

## 7.4 Newsletter

- Proposal for Mark to take over authoring newsletter, based on his Dbn output
- Renate offered to send and maintain MailChimp

---

# 8. Any Other Business

- Riaan: Email address so that members can reply to communications via a dasa.org address

Minutes:


---

# 9. Not Broadcast or Recorded

Notes:

- Any sensitive topics that need to be held in confidence.
- No recording
- Minutes to be kept in writing only
- Example of such items are ones discussed with relation to our constitution:
-- 7: Governing Board
-- 7.11: Confidentiality
- All matters pertaining to litigation, security measures, contractual negotiations, employment matters and any other matters deemed confidential by the Governing Board, must be treated as confidential and only the actual decisions may be disclosed to the general public.

Minutes:

---

# 10. Review Tasks Arising from this Meeting
## Secretary

A quick review of tasks assigned at this meeting.

### Johan
- [ ] Investigate conferencing facilities for AGM
- [ ] Add Mark Pape to contact lists / DASA calendar
- [ ] Write a thank you letter to Yonder (?) for Pta meetup
- Brought over:

### Ingrid
- [ ] Add March tasks to this list
- [ ] Create register of members on Google Docs
- Brought over:
- [ ] Circulation of constitution for signature by 2015/16 governing board - Update, Print, Sign - Ingrid
- [ ] Ingrid & Renate’s Drupal.org details to be added to the internal members list
- [ ] Update the document to create version 1.0.1 - with “adopted on” date - Date to be confirmed

### Renate
- [ ] Send backdated meetup headcount to Riaan

### Riaan 
- [ ] Send Ingrid yaml file of all members, for adding to Google Doc

### Jason
- [ ] Send backdated meetup headcount to Riaan
Brought over:
- [ ] Audit all social channels

### Lee
- [ ] Create a SysAdmin list and add to the next agenda

### All
- [ ] Add their events to the DASA Calendar
- [ ] Forward any important info to info@dasa.org


Minutes:


---
