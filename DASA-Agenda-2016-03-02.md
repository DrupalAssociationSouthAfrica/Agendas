Drupal Association South Africa
===============================

Agenda 2016-03-02
-----------------

1.1. Attending / Apologies
--------------------------

    Person    Attending ✔ / ✘ Apologies
    ===================================
    
    Governing Board
    
    Greg McKeen             ✔
    Ingrid (Inky) Talbot    ✔
    Jason Lewis             ✔
    Johan du Bois           ✔
    Lee Taylor              ✔
    Renate Ehlers           ✔
    Riaan Burger            ✔
    
    Regular Attendees
    
    Adam Dunn

Quorum achieved (five of governing board): ✔

Apologies:

Minutes:
- Invitations to meetings to be added to contrib mailing list
- Regular attendees will be added if they join

--------------------------------------------------------------------------------

1.2 Housekeeping *Johan / Inky*
-----------------------------------------------
Agenda:

- Approval of minutes of previous meeting [https://gitlab.com/DrupalAssociationSouthAfrica/Agendas/blob/master/DASA-Agenda-2016-01-20.md]
- Acceptance of agenda/Matters arising
- Confirm Next Meeting
- Rationalisation of Google Docs permissions

Minutes:
- Previous minutes: Riaan proposed, seconded by Johan
- Next meeting: 6 April
- Google Docs: Public folder and Internal folder (governing board and collaborators only)


1.3. Constitution *Ingrid / Jason*
-----------------------------------------------
Agenda:

- Governing Board: Register of Members status
- Circulation of constitution for signature by 2015/16 governing board

Minutes:
- Governing Board: Register of Members revised on Google Docs.
- Ingrid & Renate’s Drupal.org details not added. (Inky to action)
- Still to update, print, sign Constitution (Inky to action)
- Need to update to create version 1.0.1 - with “adopted on” date (need to find the date)



1.4. Handover *Adam -> Renate* / *Greg -> Inky*
-----------------------------------------------

Agenda:

- Adam > Renate
-- Treasurer and bank account.
-- Adam transfer to Renate.
- Big FNB failure with downed computers caused all of us to lose internet banking access to the account. Riaan will go figure out what to do.

- Greg > Inky
-- Secretary and documentation.
-- Greg will courier documentation to Inky.
-- Riaan to submit a narrative report.
-- Adam and Renate to submit a financial report.
-- Inky / Greg to submit new board to NPO office.
-- NPO certificate has arrived (?), will be sent along with Constitution and full package of documentation to Cpt.
-- Ingrid to submit to NPO when certificate arrives

Minutes:

- Johan to play courier on 11th March 


1.6. NPO Registration *Inky / Jason*
-----------------------------------------------

Agenda Notes:

- We have a registration!

Tasks:
- Greg to collect our forms.
- Registration approved on 8 September 2015.
- Reference Number: 158-394 NPO

Once registered:
- Update stationery and website and announce. [done]
- Adam / Treasurer will arrange an accountant (CA).

Annually:
- “Once the organisation is registered, it is obligated, in terms of sections 18 and 19, to submit within nine (9) months after the end of its financial year, annual reports (a narrative report, annual financial statement and an accounting officer’s report) including any changes to the organisation’s constitution, physical address and office bearers. “ - http://www.dsd.gov.za/npo/
- Financial reports signed by accounting officer.
- Changes to board / leadership / constitution should be noted.

Minutes:

- Accountant to be arranged by treasurer
- NPO registration / report must be submitted 9 months from Feb

1.7. Metrics for Measuring DASA's Success 2015/16 *Johan / Riaan*
-----------------------------------------------

Agenda Notes:

- Reference:
- Indicators on GitLab

Tasks:

- Johan and Riaan to confirm they and two other people can update metrics.
-- Nearly a full year of data - Riaan to present when 12 months data exists
-- End of April will have full year
- Reporting for 2014-15 to be adopted at AGM within 6 months of financial year end (i.e. before end August):
- Narrative report (Chairman)
- Financial report (Treasurer)
- Our AGM is after this date, can we adopt this at a regular meeting or SGM?
- Outgoing board will need to adopt the report, as they are responsible for it.

Minutes:

- Hold over till May meeting

1.8. Membership * N/A / N/A*
-----------------------------------------------

Agenda Notes:

- New membership structure is Individual R 150 and Organisation R 5 000.
- New creative from Jason.
- Website updated, let's promote and sign up!
- It is important to promote and drive membership, but first we need more Company members before driving Individual members
- Some organisations may be more keen to sign up when NPO registration is complete

Tasks:

- Everyone to promote and sign up!

Minutes:

- Process of membership signing up 
-- payment/invoice process to be defined
-- welcome pack (pdf, icons, etc)
-- adding to website
- Add to an internal doc with list of current members - and period - currently on YAML / Jekyll - one source 
- Show members what the fees will be spent on
- Renate & Inky to collaborate on next membership request - and document the process


1.9. Pastel Online *Renate / Riaan*
-----------------------------------------------

Agenda Notes:

- Renate has bought Pastel and will be linking it up
- Renate and Riaan to have initial login accounts.
- Bank account handover is done 

Minutes:

- All complete



========================================================================
2. National User Group Meet-ups
-----------------------------------------------
2.1. General
-----------------------------------------------
Agenda:
- Meetups to be listed on Google Docs.

Minutes:


2.2. Cape Town *Jason / N/A*
-----------------------------------------------

Minutes:

- 30th March at Quirk
- DrupalCamp Cape Town venue confirmed at Quirk as well - date tbd

2.3. Durban *Riaan / N/A*
-----------------------------------------------

Agenda Notes:

- Riaan to help local Blaize, Renier and Mark start self-organise in 2016.

Minutes:
- Starting with a quarterly meetup to ease into it
- April 6th, July 6th, October 5th
- Venue, food, sponsors confirmed


2.4. Johannesburg / Bryanston *N/A / N/A*
-----------------------------------------------

Agenda Notes:

- No venue
- Restart in March/April.
- No organiser/host either.

Minutes:

- Renate has requested that recurring event dates be removed from Meetup
- Need to inform people sooner that no event currently happening
- Need to get a new host in order to move forward

2.5. Johannesburg / Parkhurst *Riaan / N/A*
-----------------------------------------------

Minutes:
- Next meetup: 18th March


2.6. Pretoria / Centurion *Renate / N/A*
-----------------------------------------------

Minutes:

- Feb meetup a bit disappointing with turnout
- Next meetup: 10th March
- Working hard to encourage more members

2.7. Pretoria / Cape Town (NGO/Government) *Riaan / Johan*
-----------------------------------------------

Agenda Notes:

- Riaan: Will start in March, and will cycle quarterly with Cape Town.

Minutes:
- Invites have been sent, people signing up
- Cape Town September event to be held at WCGov


========================================================================

3. Communication
-----------------------------------------------
3.1. Newsletter (MailChimp) *N/A / N/A*
-----------------------------------------------

Tasks:

- Jason to contact Robin to talk about the newsletter management. We need someone that will attend the DASA meetings.
- N/A to post an article on groups.drupal.org to explain reasons to subscribe to the newsletter:
- To be notified about the elections
- Keep abreast with community news, and meetup dates
- Solicit articles
- Describe frequency of e-mails
- N/A is rebuilding the newsletter with information learnt recently.
- N/A is adding a signup form to the website.
- N/A will push the development to the live site.
- N/A will require content, job openings, meetups, calls for sponsors, write up post-meetup, pictures and photos, and to promote the fact that people can vote.

Minutes:

- Newsletter to be used for notifications - eg Camps, etc - and not a monthly communication

3.2. dasa.org.za & drupalcamp.co.za *Ingrid / N/A*
-----------------------------------------------

Agenda Notes:

- Gov/NGO Request for Forums and Private Threads.

Tasks:

- Jason to make a statement to contrib@dasa.org.za regarding the drupalcamp.co.za domain ownership. Registered ownership can by with anyone prepared to change ownership of the domain under direction of DASA if requested.
- Alternatively, looks like Jason to organise the transfer to DASA (Lee).
- Consider someone suited to the position asking if DASA can purchase the Drupal domains for South Africa, drupal.co.za and drupal.org.za, from it's current owner.
- Ingrid to contact owners of domains and see if they would be willing to transfer domains to DASA Committee
- In 2016, integrate with social channels so that we don't rely on e-mail, meetup.com and GDoSA alone. Unify.
- Jason to do an audit of all social channels/accounts - on Google Docs
- To have access to all of these channels and share with Lee

- As per Gov/NGO request, need to look at building a new website


Minutes:
- Transfer request for Lee to take over drupalcamp.co.za
- Amazee Labs will continue to host it on amazee.io
- Inky still to content owners of other domains - to be actioned
- Jason to audit all social channels - to be actioned

    

========================================================================

4. Administrative
-----------------------------------------------
4.1. Accounting *Renate / Riaan*
-----------------------------------------------

Bank Account:

    Name: Drupal Association South Africa
    Bank: FNB
    Branch: 251655
    Type: Cheque
    Number: 62446745492

Account transactions during the past month:

                             
Date
Amount
Balance
Description
2016/03/01
100
36831.32
SCHEDULED PYMT FROM DANIEL DONATION
2016/03/01
100
36731.32
SCHEDULED PYMT FROM RIAAN DONATION
2016/02/26
150
36631.32
mmbshp Dane Rossenrode
2016/02/11
-65
36481.32
#MONTHLY ACCOUNT FEE
2016/02/08
100
36546.32
SCHEDULED PYMT FROM GREG MCKEEN
2016/02/01
100
36446.32
SCHEDULED PYMT FROM DANIEL DONATION
2016/02/01
100
36346.32
SCHEDULED PYMT FROM RIAAN DONATION


Notables since previous agenda:

    Balance: R 36 246.32
    November:
        R 150 from Inky for T-shirt
        R 150 from Daniel (Burtronix) for Membership
        R 150 from Wilhelm (Burtronix) for Membership
        R 5 000 from Burtronix for Membership
        R 150 from Charlie (Rogerwilco) for Membership
        R 5 000 from Rogerwilco for Membership
    December
        R 100 from Greg for Donation
        R 300 from Lee for Donation
    January
        -R 1 998 for Pastel Online Accounting
        R 100 from Greg for Donation
        R 300 from Lee for Donation

Tasks:

- Treasurer to update this a couple of days before each meeting.
- Lee to set up a GitLab account for DASA.
- Accounting to move to a private repo on GitLab.

Minutes:
- Renate to update last financial year (and more) onto Pastel in order to generate backdated reports and create budgets
- CSV export from GitLab
- Note difference between donation and membership
- “Friends of DASA” - from donations


4.2. Any Other Business *Everyone*
-----------------------------------------------

≈ 15 min

Agenda Notes:

- Anything arising at the meeting or not on the agenda.
- DASA Meetups.com

Minutes:
- Consider Google Docs vs GitLabs > combine everything in one system
-- what is needed from a document repository?
-- can everyone please make note of what they each need

- DASA Meetup Group
- Google calendar service for monthly meetings
- Current meetup group has various members
- Will need to phase it out by end of 2016  - inform users to follow different groups
-- Riaan and Jason to collaborate

- Agenda
-- Following month’s agenda to be created a day or two after each meeting so that community can add to it

- Quick communication during the week between board members
-- Slack channel proposed - currently being used by some members, anyone welcome to join if they wish
-- Nothing being adopted

========================================================================

5. DrupalCamp Gauteng 2016 *Riaan / Renate*
-----------------------------------------------

Agenda Notes:

- Riaan to confirm date and venue
- May 2016
- Department of Arts and Culture, Arcadia, Pretoria
- Trouble confirming venue means we're considering a May date and an alternative venue.
- Most other aspects already lined up and ready to go
- GitLab sponsorship opportunity [Lee]

Minutes:
- Goethe Institute
- Site almost ready to go live
- Lee would like some formal communication to send to GitLab for sponsorship



========================================================================

6. DrupalCamp Cape Town 2016 *Jason / Johan*

Agenda Notes:

- Any early ideas.
- Authoritative organiser?

Minutes:
- Possible dates: 2nd Sept / 21st October
- Request for talks submissions / topics
- Venue: Quirk
- Combine with Gov Meetup?


========================================================================

7. Not Broadcast or Recorded
- Any Sensitive Topics Everyone

Agenda Notes:

- Any topics that need to be held in confidence.
- No recording and minutes to be kept in print / writing only.
- Example of such items are ones discussed with relation to our constitution:
- 7: Governing Board
- 7.11: Confidentiality
- All matters pertaining to litigation, security measures, contractual negotiations, employment matters and any other matters deemed confidential by the Governing Board, must be treated as confidential and only the actual decisions may be disclosed to the general public.

Minutes:


========================================================================
