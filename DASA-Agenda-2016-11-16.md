Drupal Association South Africa
===============================

Agenda 2016-11-16 from 19:30 - 21:00
----------------------------

[Hangout](https://plus.google.com/hangouts/_/dasa.org.za/dasa)

1.1. Attending / Apologies
--------------------------

    Person    Attending ✔ / ✘ Apologies
    ===================================

    Governing Board

    Greg McKeen             ✔ / ✘
    Ingrid (Inky) Talbot    ✔ / ✘
    Jason Lewis             ✔ / ✘
    Johan du Bois           ✔ / ✘
    Lee Taylor              ✔ / ✘
    Riaan Burger            ✔ / ✘

    Additional Invitees
    Mark Pape		    ✔ / ✘
    Sergio Henriques        ✔ / ✘
    

Quorum Achieved: ✔ / ✘

A quarter of the members of the governing board. CIEL(6/4) = at least 2 members.

Apologies:


--------------------------------------------------------------------------------

1.2. Review & Acceptance of Minutes
-----------------------------------

Minutes of Previous Meeting(s): 
- https://gitlab.com/DrupalAssociationSouthAfrica/Agendas/blob/master/DASA-Agenda-2016-09-21.md

Minutes:

Completed / Agreed: ✔ / ✘

--------------------------------------------------------------------------------

1.3. Discuss & Confirm Agenda
-------------------------

Minutes:

Completed / Agreed: ✔ / ✘

================================================================================

2. Finances
===========

--------------------------------------------------------------------------------

2.1. General
------------
- SAGE Online reconciled with bank account. Reports available in Internal folder on Google Drive.
  - Balance: R 38,241.28
- All outstanding DCG2016 Sponsorships have been settled.
  - Discussion of additional expenses (cash to speaker) for DCG2016 can proceed.

Minutes:

Completed / Agreed: ✔ / ✘

--------------------------------------------------------------------------------

================================================================================

3. Drupal Camp Cape Town 2016
===============================================

Minutes:

Completed / Agreed: ✔ / ✘

================================================================================

4. WAY FORWARD
===================

--------------------------------------------------------------------------------

4.1. Where we stand
------------

Minutes:


Completed / Agreed: ✔ / ✘

--------------------------------------------------------------------------------

4.2. Proposals
------------

Minutes:

Completed / Agreed: ✔ / ✘

--------------------------------------------------------------------------------

4.3. Decisions on Way Forward
------------

Minutes:

Completed / Agreed: ✔ / ✘

========================================================================

5. Any Other Business
=====================

Agenda Notes:

- Anything arising at the meeting or not on the agenda.

Minutes:

-

Completed / Agreed: ✔ / ✘

================================================================================

7. Not Broadcast or Recorded
============================

--------------------------------------------------------------------------------

Any Sensitive Topics *Everyone*
-------------------------------

Agenda Notes:

- Any topics that need to be held in confidence.
- No recording and minutes to be kept in print / writing only.
- Example of such items are ones discussed with relation to our constitution:
  - 7: Governing Board
    - 7.11: Confidentiality
      - All matters pertaining to litigation, security measures, contractual
        negotiations, employment matters and any other matters deemed
        confidential by the Governing Board, must be treated as confidential
        and only the actual decisions may be disclosed to the general public.

Minutes:

-

Completed / Agreed: ✔ / ✘

================================================================================

7. Template Vote Block
======================

Voted on: ((Statement))

    Greg McKeen             ✔ / ✘
    Ingrid (Inky) Talbot    ✔ / ✘
    Jason Lewis             ✔ / ✘
    Johan du Bois           ✔ / ✘
    Lee Taylor              ✔ / ✘
    Riaan Burger            ✔ / ✘

================================================================================
