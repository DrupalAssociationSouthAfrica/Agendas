Drupal Association South Africa
===============================

Minutes 2016-09-21 from 19:30 - 20:36
----------------------------

[Hangout](https://plus.google.com/hangouts/_/dasa.org.za/dasa)

1.1. Attending / Apologies
--------------------------

    Person    Attending ✔ / ✘ Apologies
    ===================================

    Governing Board

    Greg McKeen             ✔
    Ingrid (Inky) Talbot    ✘
    Jason Lewis             AWOL ;-)
    Johan du Bois           ✔ 
    Lee Taylor              ✘
    Riaan Burger            ✘

    Guest Attendees
    Mark Pape


Quorum Achieved: ✔

A quarter of the members of the governing board. CIEL(6/4) = at least 2 members.

Apologies:
- Riaan, Lee, Ingrid


--------------------------------------------------------------------------------

1.2. Review & Acceptance of Minutes
-----------------------------------

Minutes of Previous Meeting(s): 
- https://gitlab.com/DrupalAssociationSouthAfrica/Agendas/blob/master/DASA-Agenda-2016-08-24.md

Minutes:
- Proposed: Greg
- Seconded: Johan

Completed / Agreed: ✔

--------------------------------------------------------------------------------

1.3. Confirm Next Meeting
-------------------------

Minutes:
- Wednesday 19th October, 7:30pm, Online

Completed / Agreed: ✔

================================================================================

2. Finances
===========

--------------------------------------------------------------------------------

2.1. General
------------
- SAGE Online reconciled with bank account. Reports available in Internal folder on Google Drive.
  - Balance: R 76,048.51
- All outstanding DCG2016 Sponsorships have been settled.
  - Discussion of additional expenses (cash to speaker) for DCG2016 can proceed.
- We have an unallocated payment for SYW (see statement report in Internal folder on Google Drive). 

Minutes:
- We ask that Riaan follows up with SYW. Email sent.

Completed / Agreed: ✔

--------------------------------------------------------------------------------

2.2. Bookkeeping
----------------

Johan: No suggested book keepers received. Have identified some in CT but not yet contacted. Will advise by email. 

Minutes:

Completed / Agreed: ✔ 

================================================================================

3. Drupal Camp Cape Town 2016
===============================================

Minutes:
- FaceBook page is up; 12 speakers have been secured. 


Completed / Agreed: ✔

================================================================================

4. AGM 
===================

--------------------------------------------------------------------------------

4.1. Legal Advice
------------

- Johan: pending; will advise by email. This process can run parallel to online elections. 

Minutes:


Completed / Agreed: ✔

--------------------------------------------------------------------------------

4.2. Elections
------------

-  November:  AGM/Formal Handover to New Governing Board (notice as per constitution)
- <31 October:   Publish results
-  19th October 23:59 (midnight):   Close voting
-  28 September: Open voting
-  21 September:
  - Close acceptance of new candidates.
  - Administrative: Gather short bio of all candidates, prepare to open vote.
- 31 August - 21 September: Call for candidates.
  - Post GDoSA
  - Email/Tweet/etc. referring to post on GDoSA

Minutes:
- One one person has expressed interest in standing for DASA governing board election.
- Mark's interest is more about business development and own academic interests so he's not available. 
- Greg's view is that there is value in collaboration but we don't have enough volunteers to keep on running this. He sees the lack of interest as a signal about the community's need/demand for this organisation. One driver of DASA was to bring DrupalCon to Africa but this doesn't feel realistic nor is it clear that a DASA type entity is required. He proposes we disolve DASA but only after DrupalCamp Cape Town.
- We have to say something on groups.drupal.org soon; but, first we'll consider the issue amongst ourselves via e-mail. 


Completed / Agreed: ✔

========================================================================

5. User Group Meet-ups
-----------------------------------------------

- Johannesburg / Bryanston meetup is dormant.
- Gov/NGO meetup is dormant (try to integrate into camps for now/conduct online). 

5.1. Cape Town *Jason*
-----------------------------------------------

Minutes:
- Last month was great. Johan is speaking next week.
- There won't be a meetup in October.

Completed / Agreed: ✔


5.2. Durban *Mark*
-----------------------------------------------

Agenda Notes:

Minutes:
- Trying to a different venue so changing the day for this quarter (4th of October). New proposed venue has lots of meetups. Mark will update Google Calendar and post on Drupal Groups South Afica.
- We discussed Drupal Distributions and what could be in a talk about them.

Completed / Agreed: ✔


5.3. Johannesburg / Parkhurst *Riaan*
-----------------------------------------------

Minutes:
- We are not aware of another meetup being planned.

Completed / Agreed: ✔


5.4. Pretoria / Centurion *Renate*
-----------------------------------------------

Minutes:
- We are not aware of another meetup being planned.


Completed / Agreed: ✔

================================================================================

6. Any Other Business
=====================

Agenda Notes:

- Anything arising at the meeting or not on the agenda.

Minutes:

-

Completed / Agreed: ✔ / ✘

================================================================================

7. Not Broadcast or Recorded
============================

--------------------------------------------------------------------------------

Any Sensitive Topics *Everyone*
-------------------------------

Agenda Notes:

- Any topics that need to be held in confidence.
- No recording and minutes to be kept in print / writing only.
- Example of such items are ones discussed with relation to our constitution:
  - 7: Governing Board
    - 7.11: Confidentiality
      - All matters pertaining to litigation, security measures, contractual
        negotiations, employment matters and any other matters deemed
        onfidential by the Governing Board, must be treated as confidential
        and only the actual decisions may be disclosed to the general public.

Minutes:

-

Completed / Agreed: ✔ / ✘

================================================================================

7. Template Vote Block
======================

Voted on: ((Statement))

    Greg McKeen             ✔ / ✘
    Ingrid (Inky) Talbot    ✔ / ✘
    Jason Lewis             ✔ / ✘
    Johan du Bois           ✔ / ✘
    Lee Taylor              ✔ / ✘
    Riaan Burger            ✔ / ✘

================================================================================
