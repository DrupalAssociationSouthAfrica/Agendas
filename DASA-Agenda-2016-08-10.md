Drupal Association South Africa
===============================

Agenda 2016-08-10 from 19:00
----------------------------

[Hangout](https://plus.google.com/hangouts/_/dasa.org.za/dasa)


1.1. Attending / Apologies
--------------------------

    Person    Attending ✔ / ✘ Apologies
    ===================================

    Governing Board

    Greg McKeen             ✔ 
    Ingrid (Inky) Talbot    ✔
    Jason Lewis             ✔
    Johan du Bois           ✔
    Lee Taylor              ✔
    Riaan Burger            ✔

    Guest Attendees
    
    Ilsoda Musa

Quorum Achieved: ✔

A quarter of the members of the governing board. CIEL(6/4) = at least 2 members.

Apologies:
	None


--------------------------------------------------------------------------------

1.2. Review & Acceptance of Minutes
-----------------------------------

[Minutes of Previous Meeting](https://gitlab.com/DrupalAssociationSouthAfrica/Agendas/blob/master/DASA-Agenda-2016-07-20.md)

Minutes:
- Minutes will be reverted to the version as submitted by the secretary
- Changes proposed by Johan and Greg to be discussed at next meeting

Completed / Agreed: ✔

--------------------------------------------------------------------------------

1.3. Confirm Next Meeting
-------------------------

Minutes: 24 August, 7:30pm

Completed / Agreed: ✔


--------------------------------------------------------------------------------

================================================================================

2. Finances
===========

--------------------------------------------------------------------------------

2.1. General Finances
----------------------------------------------------
- SAGE Online reconciled with bank account. Reports available in Internal folder on Google Drive.
  - Balance: R 62,675.25
  - DCG2016 Sponsorships Outstanding: R10,000 
- Johan has been added as authorised representative on FNB account. Instruction for Riaan and Renate to be removed has been given. 
  - FNB has requested confirmation by all governing board members.
  - Reimbursable travel expenses for Johan: R1,484.75 (reimbursement form & invoices on Google Drive). 
- Riaan indicated he is only able to assist for 1 hour per month; Johan would like someone who can commit more time to assist with treasurer role.

Minutes:
- FNB requires an additional form signed, which will be made available on Google Drive
- Inky has been asked to sign the reimbursement voucher for Johan
- Jason to help Johan as co-treasurer

Completed / Agreed: ✔ 

--------------------------------------------------------------------------------

2.2. DrupalCamp Gauteng 2016: Sponsorship Outstanding
----------------------------------------------------

Notes:

Some sponsors have not paid yet:

- SYW, R 5 000
- Yonder, R 5 000
- Total: **R 10 000**

Minutes:
- Riaan has not heard from Yonder further yet, SYW are in touch

Completed / Agreed: ✔ 

--------------------------------------------------------------------------------

2.3. Bookkeeping
----------------

It has been agreed that we should throughly review books, in preparation for auditing as is required by constitution, before handing over to the next governing board. 

- Appointing a bookkeeper
  - Does Riaan's offer still stand & what would it entail?
  - Significant questions for book keeper:
    - How to handle VAT

Minutes:
- Johan has done an initial SAGE report for the past few years, but would appreciate a bookkeeper to look over the numbers
- Riaan has offered his accountant to have a look
	- Johan will make contact with her (Riaan will forward her contact details), to help provide answers on VAT and SAGE issues
	- If nothing happens with her by next Wednesday, Johan can look for help elsewhere
- Riaan has proposed that SAGE be the single source of truth for invoices going forward
	- All future invoices to be generated from SAGE
	- Meeting agreed that invoices also be exported and saved to the Google Drive (rather than archive.git)
	- While moving the files to Google Drive the meeting agreed that letters of invitation (the only other docs in that repo) also be moved to Google Drive. Changes made and pushed during meeting.

Completed / Agreed: ✔ 

================================================================================

3. Drupal Camp Cape Town 2016
===============================================

- Site needs some updating
- 10 sign ups on Quicket, before any marketing
- R39 000 in committed in sponsorship so far
  - Invoices and statements have been sent


================================================================================

4.  AGM
===============================================

From constitution:

"Annual General Meetings: All Annual General Meetings (AGMs) must be held within six months of DASA’s financial year-end. At least twenty-one days’ written notice must be given to all members stating the date, time, place and business of the AGM, which business must include:
- The Chairperson’s report,
- The presentation of DASA’s Annual Financial Statements,
- The election of Governing Board members,
- The appointment of Auditors, and
- Other appropriate matters."

Request for Legal advice:
- Are we properly constituted, are we competent to make decisions? How can we rectify this if not.
- We have this group that we consider who we represent / who are our members
	- How do we define our members, how should this be compiled and registered?
	- From GDoSA
	- Sold to individuals and companies - do we consider paid members to have any special rights?
- Can we claim to "represent" every Drupal user in South Africa?
- What is our legal responsibility to the paid members?

- Johan has been given mandate to approach lawyer who helped put the constitution together, else a lawyer Johan has contact with via an NGO
	- Will return to the next meeting with a quote

--------------------------------------------------------------------------------

4.1. Definition of All Members
-------------------------------------------

- Johan to consult legal advice on this.

-- 

4.2. Appointment of Auditors 
-------------------------------------------

- Standover pending book keeping.

--

4.3. Election of Governing Board 
-------------------------------------------

- Preliminary date for opening for call of candidates: 31 August
- If further postponements, that will be communicated by the 31st August, with reasons why.

--

4.4. Amendments to Constitution 
-------------------------------------------

- Standover pending legal advice.


================================================================================

5. Any Other Business
=====================

Agenda Notes:

- Anything arising at the meeting or not on the agenda.

Minutes:

-

Completed / Agreed: ✔ / ✘

================================================================================

6. Not Broadcast or Recorded
============================

--------------------------------------------------------------------------------

Any Sensitive Topics *Everyone*
-------------------------------

Agenda Notes:

- Any topics that need to be held in confidence.
- No recording and minutes to be kept in print / writing only.
- Example of such items are ones discussed with relation to our constitution:
  - 7: Governing Board
    - 7.11: Confidentiality
      - All matters pertaining to litigation, security measures, contractual
        negotiations, employment matters and any other matters deemed
        confidential by the Governing Board, must be treated as confidential
        and only the actual decisions may be disclosed to the general public.

Minutes:

-

Completed / Agreed: ✔ / ✘

================================================================================

7. Template Vote Block
======================

Voted on: ((Statement))

    Greg McKeen             ✔ / ✘
    Ingrid (Inky) Talbot    ✔ / ✘
    Jason Lewis             ✔ / ✘
    Johan du Bois           ✔ / ✘
    Lee Taylor              ✔ / ✘
    Riaan Burger            ✔ / ✘

================================================================================
