
# DASA DrupalCamp Gauteng Invoices for Expenses


    2016-05-13  -23 135.40  R016EJ3TP0 CHIMEZIE OSONDU

Grouping: Speakers

File: [speaker-chimezie-expenses.pdf](speaker-chimezie-expenses.pdf)


    2016-05-13     -120.00  #R016EJ3TP0 SWIFT FEE

Grouping: Speakers

File: N/A: Bank Fees


    2016-05-13     -155.00  #R016EJ3TP0 SWIFT COMMISSION

Grouping: Speakers

File: N/A: Bank Fees


    2016-05-17     -318.47  ZBD FX IBG5G03027 SD3GRF4QF6EJ3TP1

Grouping: Speakers

File: N/A: Bank Fees


    2016-05-18   -2 907.00  BANNER DRUPAL CAMP G

Grouping: Print

File: **Need from Renate**


    2016-05-19   -1 740.54  MINUTEMAN PRESS

Grouping: Print

File: [paid-2016-05-19-print-material-1.pdf](paid-2016-05-19-print-material-1.pdf)


    2016-05-19   -2 304.15  TEMPOLABELS

Grouping: Print

File: [paid-2016-05-19-print-material-2.pdf](paid-2016-05-19-print-material-2.pdf)


    2016-05-20     -900.00  BANNER CREATIVES

Grouping: Print

File: **Need from Renate**


    2016-05-23   -6 609.43  DRUPAL CAPM GATENG -

Grouping: Catering

File: [paid-2016-05-23-catering-food-day-1.pdf](paid-2016-05-23-catering-food-day-1.pdf)


    2016-05-23   -6 094.37  DRUPAL CAPM GATENG 2

Grouping: Catering

File: [paid-2016-05-23-catering-food-day-2.pdf](paid-2016-05-23-catering-food-day-2.pdf)


    2016-05-23     -313.75  MINUTEMAN 1084

Grouping: Print

File: [paid-2016-05-23-print-material.pdf](paid-2016-05-23-print-material.pdf)


    2016-05-25     -485.64  CATERING

Grouping: Catering

File: [paid-2016-05-25-catering-food-alteration.pdf](paid-2016-05-25-catering-food-alteration.pdf)


    2016-05-25   -4 531.06  SPEAKER R2K

Grouping: Speakers

File: [paid-2016-05-25-speaker-murray-expenses.pdf](paid-2016-05-25-speaker-murray-expenses.pdf)


    2016-05-25     -700.00  DI BOX BAND

Grouping: Entertainment

File: [paid-2016-05-25-entertainment-di-box.pdf](paid-2016-05-25-entertainment-di-box.pdf)


    2016-05-26   -15 000.00  BLACK MAJOR

Grouping: Entertainment

File: [paid-2016-05-26-entertainment-band.pdf](paid-2016-05-26-entertainment-band.pdf)


    2016-05-27    -1 200.00  INSTRUMENT RENTAL

Grouping: Entertainment

File: [paid-2016-05-27-entertainment-instrument.pdf](paid-2016-05-27-entertainment-instrument.pdf)


    2016-06-02     -8 597.9  EXPENSES GOETHE INST

Grouping: Catering

File: [paid-2016-06-02-catering-venue.zip](paid-2016-06-02-catering-venue.zip)


