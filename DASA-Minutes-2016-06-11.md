# Drupal Association South Africa

Special board meeting held in connection with the planning workshop.

## 2016-06-11 14:51

----

# 1. Attendance

In attendance in the Johannesburg location
    Riaan Burger
    Lee Taylor
    
In attendance in the Cape Town location
    Jason Lewis
    Johan Du Bois
    Renate Ehlers
    Greg Mckeen
    Ingrid Talbot   

    Quorum achieved (five of governing board): ✔

Guest Attendees: 
    Nelly Moseki
    Steve (surname?)
    Chris Kriel
    Duncan Moore
    Sergio Henriques
    Ferline Tiard (as facilitator for Workshop aspect of the day)
    
----

# Minutes

- Renate has resigned verbally, which we have accepted - written will be submitted
- For the interim period, Johan will take on treasurer role as well as chair, until the situation can be revised
- Riaan to sort out finances by next board meeting
- Riaan will talk to FNB to transfer into Johan's name
- The next board meeting will be scheduled once account is transferred, and a written action plan and invoices from DrupalCamp Gauteng are sorted
- Each member to send written notification of status of membership within one week
- Election status may change
- Mandate may need to change 

- Adjourned 15:03

----