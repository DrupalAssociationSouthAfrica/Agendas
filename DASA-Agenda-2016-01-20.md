Drupal Association South Africa
===============================

Agenda 2016-01-20
-----------------

1.1. Attending / Apologies
--------------------------

    Person    Attending ✔ / ✘ Apologies
    ===================================

    Governing Board

    Greg McKeen         ✔
    Ingrid Talbot       ✔
    Jason Lewis         ✔
    Johan du Bois       ✔
    Lee Taylor          ✔
    Renate Ehlers       ✘
    Riaan Burger        ✔

    Regular Attendees

    Adam Dunn           ✘
    Robin Prieschl      ✘
    Lauro Scott Parkin  ✘
    George Ziady        ✘
    Ilsoda Musa         ✘

Quorum achieved (five of governing board): ✔

Apologies:

- Renate submitted last minute apologies

Minutes:



--------------------------------------------------------------------------------

1.2 Housekeeping *Johan / Inky*
-----------------------------------------------

- Approval of minutes of previous meeting
- Changes to the agenda
  - Replace each Agenda Item responsible persons (next to headings) with real
    names. Use the minutes field for this as we don't alter the agenda itself.
- Confirm Next Meeting

Minutes:

- Riaan prosposed to accept the previous minutes, Jason seconded
- Next meeting to be held on Wed 17th Feb, via Google Docs

--------------------------------------------------------------------------------

1.3. Constitution *Ingrid / Jason*
----------------------------------

Agenda:

- *Ingrid* to edit a Google Docs copy of the constitution with the new
  governing board's details.
- *Everyone* to send their full legal names e-mail and telephone number to
  Ingrid.
- *Everyone* to download, sign and post original signed constitution to the
  Ingrid.

Minutes:

- Ingrid to still update the Constitution, Riaan to send details collected
- Internal Google Docs > Restricted folder currently visible to limited people - Ingrid, Johan, Lee - will contain personal details
- To be printed in Jhb, signed by all, couriered to Cpt for signing - Riaan to co-ordinate Jhb side, to send before 23rd Feb

--------------------------------------------------------------------------------

1.4. Handover *Adam -> Renate / Greg -> Inky*
---------------------------------------------

Agenda:

- Treasurer and bank account.
  - Adam transfer to Renate.
    - Big FNB failure with downed comuters caused all of us to lose internet
      banking access to the account. *Riaan* will go figure out what to do.
- Secretary and documentation.
  - Greg will courier documentation to Inky.
- *Riaan* to submit a narrative report.
- *Adam* and *Renate* to submit a financial report.
- *Inky* / *Greg* to submit new board to NPO office.

Minutes:

- NPO certificate to arrive next week, will be sent along with Constitution and full package of documentation to arrive in Cpt before 24th Feb.
- Riaan will upload progress on report to Internal Google Docs 
- Ingrid to submit to NPO when certificate arrives
- Adam and Renate still to collaborate

--------------------------------------------------------------------------------

1.5. Committees
--------------------------------------------------------------------

PROPOSED: ESTABLISH COMMITTEES
	- *Why?* To spread the work and give everyone in the community an opportunity to contribute, network, and grow together. 
	- *What is a committee?* 
	  - A body of one or more people, established by the governing board, to explore a matter or a number of related matters more deeply than would be practical/desirable for the whole governing board.
	  - Required to report to the governing board, generally only making recommendations for decision, except where empowered by the governing board to make specific decisions [subject to what bylaws allow].
	  - Consist of a chairperson, who must be a member of the governing board, and members who may or may not be members of the governing board (depending on nature of work).
	  - The chairperson is responsible for running meetings. Duties include keeping the discussion on the appropriate subject, recognizing members to speak, confirming what the committee has decided, and reporting back to the governing board.
Committees meet as needed and need not keep minutes. However, reports to the governing board should be reduced to writing either as a separate document or by inclusion the minutes of a governing board meeting. 
    - *Proposed committees*
      - Finance
      - Meetups
      - DrupalCamp JHB
      - DrupalCamp CT

Minutes:

- Johan presented the concept of committees
- Monthly feedback from Committees to be submitted 24hrs before meetings
- DrupalCamp JHB: Riaan to take the lead, will discuss with Renate and find other members
- DrupalCamp CT: Jason to take the lead, Ingrid and Johan to collaborate
- 	Will discuss potential dates before first CT meetup, open it up for inclusion from others
- Meetups Committee: for all Meetups organisers to collaborate, open a channel for assistance
- 	Each meetup will have a Board Champion - not necessarily the meetup organiser, who will represent the Meetup
- 	No specific committee needed

--------------------------------------------------------------------------------

1.6. NPO Registration  *Inky / Jason*
-------------------------------------

Agenda Notes:

- We have a registration!

Tasks:

- *Greg* to collect our forms.
- Registration approved on 8 September 2015.
  - Reference Number: 158-394 NPO
- Once registered:
  - Update stationery and website and announce.
  - *Adam / Treasurer* will arrange an accountant (CA).
  - Annually:
    - Narrative report (first one due in August by Chairman).
    - Financial reports signed by accounting officer.
    - **Changes to board / leadership / constitution should be noted.**

Minutes:

- Greg to collect forms on Tuesday
- Will be announced when we have papers in hand

--------------------------------------------------------------------------------

1.7. Metrics for Measuring DASA's Success 2015/16  *Johan / Riaan*
------------------------------------------------------------------

Agenda Notes:

- Reference:
  - [Indicators on GitHub](https://github.com/DrupalAssociationSA/InternalDocumentation/tree/master/Indicators)

Tasks:

- *Johan* and *Riaan* to confirm they and two other people can update metrics.
- *Riaan* to develop JavaScript widget for visual representation.
- Reporting for 2014-15 to be adopted at AGM within 6 months of financial year
  end (i.e. before end August):
  - Narrative report (Chairman)
  - Financial report (Treasurer)
- Our AGM is after this date, can we adopt this at a regular meeting or SGM?
- Outgoing board will need to adopt the report, as they are responsible for it.

Minutes:

- Nearly a full year of data - Riaan to present when 12 months data exists

--------------------------------------------------------------------------------

1.8. Membership *N/A / N/A*
---------------------------

Agenda Notes:

- New membership structure is *Individual R 150* and *Organisation R 5 000*.
- New creative from *Jason*.
- Website updated, let's promote and sign up!

Tasks:

- *Everyone* to promote and sign up!

Minutes:

- It is important to promote and drive membership, but first we need more Company members before driving Individual members
- Some organisations may be more keen to sign up when NPO registration is complete

--------------------------------------------------------------------------------

1.9. Pastel Online *Renate / Riaan*
------------------------------------

Agenda Notes:

- *Renate* to sign us up for Pastel Online.
  - *Renate* and *Riaan* to have initial login accounts.

Minutes:

- Bank account handover is done
- Renate has bought Pastel and will be linking it up

================================================================================

2. National User Group Meet-ups
===============================

--------------------------------------------------------------------------------

2.1. National *Jason* / *Robin*
-------------------------------

Minutes:

- No need for 2.1 / National, will go straight into groups
- Spreadsheet in Google Docs with details of Meetup organisers and Champions

--------------------------------------------------------------------------------

2.2. Cape Town *Jason / N/A*
----------------------------

Minutes:

- 24th Feb next Meetup at Roger Wilco, Durbanville (TBC)
- 	No speaker confirmed as yet
- From March will return to Bandwidth Barn, Woodstock

--------------------------------------------------------------------------------

2.3. Durban *Riaan / N/A*
-------------------------

Agenda Notes:

- *Riaan* to help local *Blaze*, *Renier* and *Mark* start self-organise in
  2016.

Minutes:

- Riaan and Blaize to discuss shortly and get 2016 going

--------------------------------------------------------------------------------

2.4. Johannesburg / Bryanston *N/A / N/A*
-----------------------------------------

Agenda Notes:

- No venue for restart in February.
- No organiser/host either.

Minutes:

- Riaan: still no venue or host, on hold for another 2 months
- 	Shouldn't have impact on DrupalCamp, but growth of Jhb members

--------------------------------------------------------------------------------

2.5. Johannesburg / Parkhurst *Riaan / N/A*
-------------------------------------------

Minutes:

- Next Meetup: first week of Feb
- Hope to absorb some of the Bryanston members

--------------------------------------------------------------------------------

2.6. Pretoria / Centurion *Renate / N/A*
----------------------------------------

Minutes:

- A successful meeting was held in January

--------------------------------------------------------------------------------

2.7. Pretoria / Cape Town (NGO/Government) *Riaan / Johan*
----------------------------------------------------------

Agenda Notes:

- *Riaan*: Will start in March, and will cycle quarterly with Cape Town.

Minutes:

- 

================================================================================

3. Communication
================

--------------------------------------------------------------------------------

3.1. Newsletter (MailChimp) *N/A / N/A*
---------------------------------------

Tasks:

- *Jason* to contact Robin to talk about the newsletter management. We need
  someone that will attend the DASA meetings.
- *N/A* to post an article on groups.drupal.org to explain reasons to
  subscribe to the newsletter:
  - To be notified about the elections
  - Keep abreast with community news, and meetup dates
  - Solicit articles
  - Describe frequency of e-mails
- *N/A* is rebuilding the newsletter with information learnt recently.
- *N/A* is adding a signup form to the website.
- *N/A* will push the development to the live site.
- *N/A* will require content, job openings, meetups, calls for sponsors, write
  up post-meetup, pictures and photos, and to promote the fact that people can
  vote.

Minutes:

- Jason still to talk to Robin
- Newsletter to be used for notifications - eg Camps, etc - and not a monthly communication
- 

--------------------------------------------------------------------------------

3.2. dasa.org.za & drupalcamp.co.za *Ingrid / N/A*
-----------------------------------------------

Agenda Notes:

- Gov/NGO Request for Forums and Private Threads.

Tasks:

- *Jason* to make a statement to contrib@dasa.org.za regarding the
  drupalcamp.co.za domain ownership. Registered ownership can by with anyone
  prepared to change ownership of the domain under direction of DASA if
  requested.
  - Alternatively, looks like *Jason* to organise the transfer to DASA (Lee).
- Consider someone suited to the position asking if DASA can purchase the Drupal
  domains for South Africa, drupal.co.za and drupal.org.za, from it's current
  owner.
- In 2016, integrate with social channels so that we don't rely on e-mail,
  meetup.com and GDoSA alone. Unify.

Minutes:

- Ingrid to contact owners of domains and see if they would be willing to transfer domains to DASA Committee
- Jason to do an audit of all social channels/accounts - on Google Docs 
- 	To have access to all of these channels and share with Lee
- As per Gov/NGO request, need to look at building a new website
- 	Will need to be totally open to all

================================================================================

4. Administrative
=================

--------------------------------------------------------------------------------

4.1. Accounting *Renate / Riaan*
--------------------------------

Bank Account:

- Name: **Drupal Association South Africa**
- Bank: **FNB**
- Branch: **251655**
- Type: **Cheque**
- Number: **62446745492**

Notables since previous agenda:

- Balance: R 36 246.32
- November:
  - R 150 from Inky for T-shirt
  - R 150 from Daniel (Burtronix) for Membership
  - R 150 from Wilhelm (Burtronix) for Membership
  - R 5 000 from Burtronix for Membership
  - R 150 from Charlie (Rogerwilco) for Membership
  - R 5 000 from Rogerwilco for Membership
- December
  - R 100 from Greg for Donation
  - R 300 from Lee for Donation
- January
  - -R 1 998 for Pastel Online Accounting
  - R 100 from Greg for Donation
  - R 300 from Lee for Donation

Tasks:

- *Treasurer* to update this a couple of days before each meeting.
- *Lee* to set up a GitLab account for DASA.
  - Accounting to move to a private repo on GitLab.

Minutes:

- Pastel licence is annual (tbc)
- Lee has set up the GitLab account, but not as relevant immediately due to Pastel
- 	Long term plan to move from GitHub to GitLab
- 	Riaan keen to look into this further

--------------------------------------------------------------------------------

4.2. Any Other Business *Everyone*
----------------------------------
*≈ 15 min*

Agenda Notes:

- Anything arising at the meeting or not on the agenda.

Minutes:

- 

================================================================================

5. DrupalCamp Gauteng 2016 *Riaan / Renate*
===========================================

Agenda Notes:

- *10 and 11 March 2016*
- *Department of Arts and Culture, Arcadia, Pretoria*
- Trouble confirming venue means we're considering a May date and an
  alternative venue.

Minutes:

- Riaan to confirm date and venue in next 2 weeks
- Most other aspects already lined up and ready to go
- Consensus that May date is more comfortable

================================================================================

6. DrupalCamp Cape Town 2016 *N/A / N/A*
========================================

Agenda Notes:

- Any early ideas.
- Authoritative organiser?

Minutes:

- More info will come after February meetup

================================================================================

7. Not Broadcast or Recorded
============================

--------------------------------------------------------------------------------

Any Sensitive Topics *Everyone*
-------------------------------

Agenda Notes:

- Any topics that need to be held in confidence.
- No recording and minutes to be kept in print / writing only.
- Example of such items are ones discussed with relation to our constitution:
  - 7: Governing Board
    - 7.11: Confidentiality
      - All matters pertaining to litigation, security measures, contractual
        negotiations, employment matters and any other matters deemed
        confidential by the Governing Board, must be treated as confidential
        and only the actual decisions may be disclosed to the general public.

Minutes:

-

================================================================================
