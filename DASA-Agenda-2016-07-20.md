Drupal Association South Africa
===============================

Agenda 2016-07-20 from 19:00
----------------------------

[Hangout](https://plus.google.com/hangouts/_/dasa.org.za/dasa)


1.1. Attending / Apologies
--------------------------

    Person    Attending ✔ / ✘ Apologies
    ===================================

    Governing Board

    Greg McKeen             ✔ 
    Ingrid (Inky) Talbot    ✔ 
    Jason Lewis             ✘
    Johan du Bois           ✔ 
    Lee Taylor              ✔ 
    Riaan Burger            ✔ 

    Guest Attendees

    	Mark Pape 

Quorum Achieved: ✔ 

A quarter of the members of the governing board. CIEL(6/4) = at least 2 members.

Apologies:

Jason, working late
-

Minutes:

-

Completed / Agreed: ✔ / ✘

--------------------------------------------------------------------------------

1.2. Review & Acceptance of Minutes
-----------------------------------

[Minutes of Previous Meeting](https://gitlab.com/DrupalAssociationSouthAfrica/Agendas/blob/master/DASA-Minutes-2016-06-11.md)

Minutes:

Previous minutes from the Meeting of the Governing Board (noted that it was not a Special General Meeting

Proposed: Riaan
Seconded: Lee

-

Completed / Agreed: ✔ 

--------------------------------------------------------------------------------

1.3. Confirm Next Meeting
-------------------------

Minutes:
- 10th August, 19:00, Online :) Invite has been created on DASA Google calendar.
	
- Johan asked for shorter meetings a bit more often over the next period in order to sort out any issues and elections upcoming

-

Completed / Agreed: ✔ 


--------------------------------------------------------------------------------

1.4. Feedback on 11 June workshop
---------------------------------

Minutes:
- Cost for the day was R4700 and was properly authorised and agreed by Renate and Johan.
- A proposal to thank attendees, facilitator, and hosts was withdrawn as it was generally felt that further discussion was not helpful. The group would rather focus on preparing for and completing the transition to new leadership. 

Completed / Agreed: ✔ 


================================================================================

2. Finances
===========

--------------------------------------------------------------------------------

2.1. General Finances
----------------------------------------------------
- SAGE Online reconciled with bank account. Reports availabe in Internal folder on Google Drive.
  - Balance: R 38,806.61
  - DCG2016 Sponsorships Outstanding: R10,000 

- Bank account still solely controlled by Riaan.

Minutes:
- 

Completed / Agreed: ✔

--------------------------------------------------------------------------------

2.2. DrupalCamp Gauteng 2016: Income and Sponsorship
----------------------------------------------------

Notes:

Income was through sponsorship. Thank you to the sponsors:

- Venue:
  - Goethe-Institut
  - Music in Africa
- Platinum at *R 15 000*:
  - SpinningYourWeb
  - Rogerwilco
  - Brave Digital
  - Burtronix
- Gold at *R 5 000*:
  - Ingen Media
  - Yonder Media
  - Telamenta
- Silver at *R 2 000*:
  - LeeNX Consultancy
  - Deep Current
- In Kind:
  - Drupal Association
  - Uber
  - Drupalize.Me
  - Right2Know Campaign
- Total: **R 79 000**

Some sponsors have not paid yet and Riaan contacted them by telephone to confirm
that payment is forthcoming:

- Brave Digital, R 15 000
  - Committed to "soon".
- SYW, R 15 000
  - Payment will be in R 5 000 instalments.
- Telamenta, R 5 000
  - Committed when cash-flow allows: "soon".
- Yonder, R 5 000
  - Committed to "soon, when their CTO returns".
- Total: **R 40 000**

Minutes:
- Outstanding amounts from Yonder and Spinning Your Web of R5000 each
- Riaan will forward documentation from Quicket for tickets sold to Johan.

Completed / Agreed: ✔

--------------------------------------------------------------------------------

2.3. DrupalCamp Gauteng 2016: Expenses
--------------------------------------

[Supporting Documentation](DASA-Agenda-2016-07-13-supporting/index.md) include a
more complete description and documentation, including invoices for each of the
expenses for the camp.

Total: **R 75 112.71**

Nigerian banks cancelled all services to their credit cards while our Nigerian
speaker was in the country. After Riaan consulted with both Greg and Renate on
the matter, he transferred R 2 000 for living expenses and transfer to the
airport to Lee who was closest to our stranded guest and then took the money to
him. Riaan still needs to be reimbursed the R 2 000.

Minutes:
- All supporting documentation has been collected (receipts and invoices as 
supplied).
- Reimbursement for Nigerian speaker support will be discussed again when all
sponsorship monies have been received.

Completed / Agreed: ✔

--------------------------------------------------------------------------------

2.4. DrupalCamp Gauteng 2016: Findings/Recommendations
------------------------------------------------------

Riaan: This is the first DrupalCamp for which each expense is documented, even
off-book expenses have receipts, all income from sponsors are logged in our
account and can be matched to a sponsor paying the amount. We could have done
even better, with documented sign-off by two members of the board for each
expense and more than one quote for each expense too, but I want to recommend
against that as a hard rule if it is at all possible. We have an upcoming camp
and I worry that such financial discipline may add too much overhead to the
organisational aspects of the camp. We trust our organisers and considering the
massive amount of attention on the Johannesburg camp just past and the amount of
reference documentation for it now in place to confirm all expenses are
documented, we haven't had nor do I expect we will have any need for it. If we
worry about expenses exceeding income, we can always create a separate account
for the camp. If we manage to get our bank account transferred to a Treasurer
correctly, that alone will create enough oversight for payments.

Johan: Provided verbal feedback on findings based on review of documentation and
discussions with Renate and Riaan.

Minutes:
- Riaan will provide a reconciled list of payments, and who approved which 
payments, endorsed by Greg and Renate as an accurate reflection. Greg has
already, during the meeting, sent a list of the transactions he approved
to core@dasa.org.za
- Based on the documents available at this time it cannot be shown that all 
transactions and financial decisions were approved by at least two members.
- The claim of "first DrupalCamp for which each expense is documented" cannot be
verified at this point. The notes above the minutes section constitute
background and further information and are not necessarily shared by entire
group. The minutes section contains agreed points.
- "Sign-off by two members of the board" is a rule, not a recommendation, and
will be adhered to stricly in future.
- Credit was extended to sponsors who requested it. However, not all sponsors or
prospective sponsors were informed that a credit option was available. The offer
of sponsorships on credit was not approved by the governing board. Riaan
acknowledges extending credit was irregular.

Completed / Agreed: ✔

================================================================================

3. Trimmed-down Agenda Until Next Elected Board
===============================================

--------------------------------------------------------------------------------

3.1. Reason for Postponement of Other Items
-------------------------------------------

We want a new mandate from our electorate through elections of a new governing
board. Our biggest questions are about:

- Hands on/off management by DASA of some Drupal community events and
  programmes.
- Potential wide-ranging changes to the way DASA operates, including potential
  changes to the constitution.

Minutes:
- The group did not agree that the abovementioned constituted the biggest
questions facing DASA. However, we did agree that we want to consider how best
to proceed to give the next governing board a good start. 
- We don't want to leave a legacy of a constitution that is hard to live up to;
and, we want to use what we have learnt from our challenges to lay the ground
work for next team.
- We need to consider all stakeholders including those we have sold memberships
to and those who participate through GDoSA.

Completed / Agreed: ✔ 

--------------------------------------------------------------------------------

3.2. Treasurer
--------------

We have an interim Treasurer in Johan since Renate resigned and we accepted her
resignation at our last meeting.

Our constitution is not clear about whether one person can hold two of the
required portfolios:

```
7.2. Number and Portfolios: A minimum of five members shall serve on the
     Governing Board bearing the following portfolios: the Chairperson,
     the Vice-Chairperson, the Treasurer, the Secretary and the Portfolio
     Manager.
```

We need to decide whether the spirit of the document requires a separate person
to hold the portfolio of the Treasurer and if so, appoint a new Treasurer.

Minutes:
- The board is happy to proceed with Johan in two roles, supported and assisted
by Riaan, until the next elections.
- Johan is to be given payment access to account by Riaan.


Completed / Agreed: ✔

--------------------------------------------------------------------------------

3.3. FNB Bank Account Transfer
------------------------------

We need to arrange transfer of the FNB bank account to our new Treasurer. Riaan
and the new Treasurer will need to be at the Fourways FNB branch to do so.

Minutes:
- Johan to fly to Jhb to meet with Riaan and take over access of the bank account
- The two approvers rule will be adhered to regarding travel expenses.  

Completed / Agreed: ✔ 


--------------------------------------------------------------------------------

3.4. Bookkeeping
----------------

Before we can have an audit, we need to have our books kept.

Riaan: I'm already requesting a bookkeeper independently. Should DASA want to
make use of the documentation I will share.

Minutes:

- Riaan has offered to get his bookkeeper to sort out the books from the past
years in order to do the audit. Next steps will be discussed at next meeting or
by email. Book keeping depends on capture of information from hardcopy into SAGE.
- Need to sort the audit out before handing over to the next board.

Completed / Agreed: ✔

--------------------------------------------------------------------------------

3.5. Audit
----------

None of us understood the implications of our constitution:

```
11.4 Financial Report: The Governing Board must ensure that proper records and
     books of account which fairly reflect the affairs of DASA are kept, and
     within six months of its financial year a report is compiled by an
     independent practicing auditor registered in terms of the Auditing
     Profession Act stating whether or not the financial statements of DASA are
     consistent with its accounting records, the accounting policies are
     appropriate and have been appropriately applied with in preparing the
     financial statements and DASA has complied with the financial provisions of
     this constitution.
```

We need to appoint an auditor for all the years of operation since our first
financial transaction on 18th of December 2013 (2013-12-18).

This can cost a substantial amount of money.

Minutes:
- This will be disucssed again when we have sorted out book keeping.

Completed / Agreed: ✔

--------------------------------------------------------------------------------

3.6. NPO Status & Audit Requirements in Constitution
----------------------------------------------------

There's a question on whether we should remove the requirements for an audit
from our constitution.

Riaan: I would prefer a newly elected board make such decisions.

Minutes:
- It was noted that being audited is not absolute requirement for NPO registration nor for doing business with government. As such Riaan no longer objects to moving to strike this requirement from constitution.  

Completed / Agreed: ✔ / ✘

================================================================================

4. 2016 Governing Board Elections
=================================

We can choose to hold early elections, then the schedule will look like this:

-  21 September: New Board
  - Assign portfolios:
    Chairman, Vice Chairman, Secretary, Treasurer, Portfolio Manager
  - Review [Constitution](https://docs.google.com/document/d/1DYY133KEg320wm4mIIBMDMd8f31LpndEimysmFUe6oU) and confirm duties
- <31 August:    Publish results
-  24 August:    Close voting
-  13 July:      Open voting
-   6 July:
  - Close acceptance of new candidates.
  - Administrative: Gather short bio of all candidates, prepare to open vote.
-  **Right Away** 6 July: Call for candidates.
- Before: Promote upcoming elections

If we do not choose to hold early elections this is the schedule:

-  17 November:  New Board
  - Assign portfolios:
    Chairman, Vice Chairman, Secretary, Treasurer, Portfolio Manager
  - Review [Constitution](https://docs.google.com/document/d/1DYY133KEg320wm4mIIBMDMd8f31LpndEimysmFUe6oU) and confirm duties
- <31 October:   Publish results
-  24 October:   Close voting
-  19 September: Open voting
-  12 September:
  - Close acceptance of new candidates.
  - Administrative: Gather short bio of all candidates, prepare to open vote.
-  17 August - 12 September: Call for candidates.
- Before: Promote upcoming elections


### Relevant Procedure

- Vote by supplying a list in order of preference of up to 7 candidates.
- Only one vote per person, the latest of multiple votes cast by one
  person invalidates the preceding ones by that person unless the latest
  is invalid, then the latest preceding valid one, if one exists will
  count.
- Secretary and Treasurer will tally the votes **< one week**. Counting 7
  points for the highest preference and one less point for every one
  thereafter per vote.
- Chairman will audit and publish the results.

- From the DASA Constitution,
  section 7: The Governing Board, subsection 4: Term of office:

  - At least one board member of the Governing Board as a volunteer to do
    so, or failing that, designated through a vote by the Governing Board
    shall retire and will not be eligible as a Governing Board member for 12
    months.

  - No member of the Governing Board may serve for more than three
    consecutive years.

    - Greg and Riaan are ineligible for candidacy.

Minutes:
- We are not yet ready to make a decision regarding when elections should occur. 
- We are agreed that we are open to considering possible changes to constitution; considerations may include:
-- Reserving a number of seats to be filled by votes of individual members, organisational members, and g.d.o votes respectively.
-- Extending and staggering board terms to ensure continuity, e.g. having two year terms with half of seats being rotated in a given year.


Completed / Agreed: ✔

================================================================================

5. Any Other Business
=====================

Agenda Notes:

- Anything arising at the meeting or not on the agenda.

Minutes:

-

Completed / Agreed: ✔

================================================================================

6. Not Broadcast or Recorded
============================

--------------------------------------------------------------------------------

Any Sensitive Topics *Everyone*
-------------------------------

Agenda Notes:

- Any topics that need to be held in confidence.
- No recording and minutes to be kept in print / writing only.
- Example of such items are ones discussed with relation to our constitution:
  - 7: Governing Board
    - 7.11: Confidentiality
      - All matters pertaining to litigation, security measures, contractual
        negotiations, employment matters and any other matters deemed
        onfidential by the Governing Board, must be treated as confidential
        and only the actual decisions may be disclosed to the general public.

Minutes:

-

Completed / Agreed: ✔ / ✘

================================================================================

7. Template Vote Block
======================

Voted on: ((Statement))

    Greg McKeen             ✔ / ✘
    Ingrid (Inky) Talbot    ✔ / ✘
    Jason Lewis             ✔ / ✘
    Johan du Bois           ✔ / ✘
    Lee Taylor              ✔ / ✘
    Riaan Burger            ✔ / ✘

================================================================================
